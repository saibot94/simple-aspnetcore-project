﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class MyApplication
    {
        const string ServerEndpoint = "https://my-test-python-app.herokuapp.com/";

        public MyApplication()
        {

        }

        public async Task<ServerResponse> CallExternalServer()
        {
            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync(ServerEndpoint);
            var responseString = await response.Content.ReadAsStringAsync();


            return JsonConvert.DeserializeObject<ServerResponse>(responseString);
        }
    }

}
