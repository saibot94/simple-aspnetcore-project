﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace WebApplication2
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseExceptionHandler("/error");

            app.Run(async (context) =>
            {
                var path = context.Request.Path;
                Console.WriteLine(path);
                if(path == "/feed")
                {
                    await context.Response.WriteAsync("This is my feed");
                } else if(path == "/" || !path.HasValue || path == "")
                {
                    var myApp = new MyApplication();
                    ServerResponse response = await myApp.CallExternalServer();
                    var result = response.Status.ToString() + response.Success.ToString();

                    await context.Response.WriteAsync(result);
                } else if (path == "/error")
                {
                    context.Response.StatusCode = 404;
                    await context.Response.WriteAsync("Sorry, couldn't find this page");
                } else
                {
                    context.Response.Redirect("/error");
                }
            });
        }
    }
}
