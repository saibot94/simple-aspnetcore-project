﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2
{
    public class ServerResponse
    {
        public string Status { get; set; }
        public bool Success { get; set; }
    }
}
